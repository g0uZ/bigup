<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/bigup-paquet-xml-bigup?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bigup_description' => '',
	'bigup_nom' => 'Big Upload',
	'bigup_slogan' => 'Transferir arquivos de qualquer tamanho'
);
