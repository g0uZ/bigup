<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/bigup-bigup?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bigup_titre' => 'Big Upload',
	'bouton_annuler' => 'Anular',
	'bouton_enlever' => 'Retirar',

	// C
	'cfg_charger_public' => 'Script na área pública',
	'cfg_charger_public_case' => 'Carregar os scripts na área pública',
	'cfg_max_file_size' => 'Tamanho máximo dos arquivos',
	'cfg_max_file_size_explication' => 'Tamanho máximo dos arquivos (em MB).',
	'cfg_titre_parametrages' => 'Configurações do Big Upload',
	'choisir' => 'Escolher',

	// D
	'deposer_la_vignette_ici' => 'Soltar a vinheta aqui',
	'deposer_le_logo_ici' => 'Solte o LOGO aqui',
	'deposer_vos_fichiers_ici' => 'Solte os seus arquivos aqui',
	'deposer_votre_fichier_ici' => 'Solte o seu arquivo aqui',

	// E
	'erreur_de_transfert' => 'Erro na transferência.',
	'erreur_probleme_survenu' => 'Ocorreu um problema…',
	'erreur_taille_max' => 'O arquivo não pode ultrapassar @taille@',
	'erreur_type_fichier' => 'Tipo de arquivo incorreto!',

	// F
	'fichier_efface' => 'Arquivo excluído',

	// O
	'ou' => 'ou',

	// S
	'selectionner' => 'selecionar',
	'succes_fichier_envoye' => 'O arquivo foi enviado',
	'succes_logo_envoye' => 'O logo foi enviado',
	'succes_vignette_envoyee' => 'A vinheta foi enviada',

	// T
	'televerser' => 'Transferir',
	'titre_page_configurer_bigup' => 'Configurar o Big Upload',

	// Z
	'zbug_necessite_plugin' => 'A tag <code>@balise@</code> requer o plugin <code>@plugin@</code>'
);
